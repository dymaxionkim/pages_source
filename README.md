# DymaxionKim's Page

* https://dymaxionkim.codeberg.page/
* https://raw.codeberg.page/dymaxionkim/pages/
* https://dymaxionkim.codeberg.page/reponame/

### Publish in MS Windows

```cmd
xcopy D:\codeberg\pages_source\public D:\codeberg\pages /E
cd D:\codeberg\pages
git add --all
git commit -m '...'
git push
```

### Setting in Arch Linux

* Environment

```bash
cd ~/codeberg
git clone https://codeberg.org/dymaxionkim/pages_source.git
git clone https://codeberg.org/dymaxionkim/pages.git ./public
ln -s ~/codeberg/pages_source/public ~/codeberg/pages
git clone git clone https://github.com/M1cR0xf7/kaslaanka.git themes/kaslaanka --depth=1
ln -s ~/codeberg/pages_source/themes/kaslaanka ~/codeberg/kaslaanka
```

* Update

```bash
cd ~/codeberg/pages_source
git pull
cd ~/codeberg/pages
git pull
cd ~codeberg/kaslaanka
git pull
```

* New Document

```bash
cp ~/codeberg/pages_source/content/blog/_index.md ~/codeberg/pages_source/content/blog/en/NEW.md
micro ~/codeberg/pages_source/content/blog/en/NEW.md
# Edit contents
cd ~/codeberg/pages_source
hugo
git commit -am "NEW"
git push
cd ~/codeberg/pages
git commit -am "NEW"
git push

```

or

```bash
cd ~/codeberg/pages_source
./new.sh
# Enter NEW.md
```

---
title: "About me"
date: "2023-04-18"
menu: "main"
description: "About"
---

![](https://avatars.githubusercontent.com/u/12775748?v=4)

## Who ?
* Name : Kim,Dong-ho
* Job : Mechanical Engineer / Mechanical Designer

## Skills ...

### CAD
* ability
 1. High quality Modeling with complex & freeformed shapes
 1. Various experiences
 1. Robots, Consumers, Military …
* Dassault Solidworks : Very good
* PTC CREO : Very good
* Dassault CATIA V5 : Good
* DraftSight (AutoCAD alternatives) : Very good
* FreeCAD (Free software in Linux desktop) : Not bad

### CAE
* ability
 1. Linear static, Transient, Buckling, Eigenmode, Harmonic …
 1.* Non-linear Multi-bodies Contact
 1.* Thermal conduction, convection, radiation, advection
 1. Fluidics with Navier-Stokes equation for laminar, boussinesq approximation, turbulance with k-epsilon model
 1. Acoustics with Helmholtz equation
* Solidworks Simulation
* CREO mechanica, dynamics
* CATIA analysis
* Pre-Processor : LS-PREPOST
* Opensource Pre-Processor : Gmsh, Salome Platform
* Opensource Solver : ElmerFEM, CalculiX, OpenRadioss
* Opensource Post-processor : Paraview

### CODING
* ability
 1. Numerical design (ex: gearbox & teeth)
 1. System analysis
* VCS : git
* Julia : Numerical design & analysis

## Interested in ...

### Gear design for robots
* Involute Profile
* Cycloid Profile
* New Reducer Design : Harmonic drive, Mini Cycloidal drive, Ball reducer, Magnetic reducer …

### Opensource CAE
* Why? : No cost & Efficient Study

### New type Manipulators
* Lightweight
* Modulized
* Affordable
* Beautiful

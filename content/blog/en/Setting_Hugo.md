---
title: "Setting_Hugo"
author: "DymaxionKim"
date: "2023-04-18"
brief: "How to set Hugo"

tags:
    - Test

meta_img: images/example.jpg
---

## Hugo
* Download https://github.com/gohugoio/hugo/releases
* Extract into the site directory : `D:\codeberg\pages`


* Initial Generate

```cmd
cd D:\codeberg
hugo new site . --force
```

## Theme : kaslaanka

* Ref : https://github.com/M1cR0xf7/kaslaanka
* Clone

```cmd
git init
git clone https://github.com/M1cR0xf7/kaslaanka.git themes/kaslaanka --depth=1
```

* Modify : `/config.toml`, `/static/css/custom.css`, `/layouts/...`


## Post

* `D:\codeberg\pages\content\blog\en\...`

* Check

```cmd
hugo server -D
```

* Publish

```cmd
hugo
```


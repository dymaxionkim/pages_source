---
title: "Using_Git_LFS"
author: "DymaxionKim"
date: "2023-04-22"
brief: "Some Examples using git LFS"

tags:
    - PDM
    - git
    
meta_img: images/example.jpg
---

## Merge in LFS

```bash
# main branch
git checkout main
# Modify files
git add --all
git commit -m "Update main"

# member branch
git checkout member
# Modify same files
git add --all
git commit -m "Update member"

# Checkout to main and Merge  member branch
git checkout main
git merge member   # if failed
git status  # Check status
git merge -Xtheirs member   # Overwright by member branch
git merge -Xours member   # Overwright by main branch
```

## git-lfs filter-process failed

```bash
git lfs install --skip-smudge
git lfs push/pull   # on situations
git lfs install --force
```

## Listing

```bash
git ls-files  # List of every files
git ls-files -mo  # List of modified files
git ls-files -mo --exclude-standard  # List of managed modified files
git ls-files --exclude-standard  # List of managed files
```

---
title: "Install_OpenRadioss_on_MS_Windows"
author: "DymaxionKim"
date: "2023-04-18"
brief: "Installation OpenRadioss Toolchains on MS Windows"

tags:
    - OpenRadioss
    - CAE

meta_img: images/example.jpg
---

## Target

* User : beginner
* OS : MS Windows
* Guide to the easiest way

## 1. Install OpenRadioss Solver

* https://github.com/OpenRadioss/OpenRadioss/releases
* Download `OpenRadioss_win64.zip`
* Extract to `D:\UTIL\OpenRadioss`

## 2. Install OpenRadioss GUI

* https://openradioss.atlassian.net/wiki/spaces/OPENRADIOSS/pages/45252609/python+tk+guis+for+job+submission+anim-vtk+conversion+and+T01-csv+conversion
* Download `Windows gui Scripts zip download`
* Extract to `D:\UTIL\OpenRadioss\win_scripts`
* Create a shortcut of `RunJobScriptWin.exe` and move it to the desktop
* Create a shortcut of `ANIM-vtkScriptWin.exe` and move it to the desktop

## 3. Install LS-PREPOST

* https://ftp.lstc.com/anonymous/outgoing/lsprepost/
* Download latest version
* Install

## 4. Install Paraview

* https://www.paraview.org/download/
* Download latest version
* A portable zip file is better
* Extract to `D:\UTIL\Paraview`
* Create a shortcut of `bin\paraview.exe` and move it to the desktop

## 5. Install Text Editor

* Recommended : MS Code editor
* Because it has LS-DYNA extension

## 6. Download Materials Library

* http://www.varmintal.com/aengr.htm


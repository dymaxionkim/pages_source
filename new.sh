#!/bin/sh

echo -n "Enter New Document File Name : " 
read DocName

cp ./content/blog/_index.md ./content/blog/en/$DocName
#micro ./content/blog/en/$DocName
code ./content/blog/en/$DocName

echo -n "Generate by Hugo"
hugo

echo -n "Push by git"
git add ./content/blog/en/$DocName
git commit -m $DocName
git push -u origin master

echo -n "Publish"
cd public
git add --all
git commit -m $DocName
git push -u origin master
cd ..

exit
